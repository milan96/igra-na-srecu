const fs = require("fs");
const path = require("path");

const { validationResult } = require("express-validator/check");
const Game = require("../models/game");
const User = require("../models/user");

exports.getUsers = (req, res, next) => {
  User.find()
    .then(users => {
      res.status(200).json({ message: "Ucitani su korisnici", users: users });
    })
    .catch(err => {
      if (err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.bilans = (req, res, next) => {
  const id = req.body.id;
  
  
  console.log(req.body);
  const bilans = req.body.bilans;

  User.findById(id)
    .then(user => {
      if (!user) {
        const error = new Error("Ne moze da se nadje korisnik");
        error.statusCode = 404;
        throw error;
      }
      user.bilans = bilans;
      return user.save();
    })
    .then(result => {
      res.status(200).json({ message: "Izvrsena je promena stanja!", user: result });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

const clearImage = filePath => {
  filePath = path.join(filePath);
  fs.unlink(filePath, err => console.log(err));
};