const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const gameSchema = new Schema({
  data: { type: String, required: true },
  player: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
});

module.exports = mongoose.model("Game", gameSchema);
