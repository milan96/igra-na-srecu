const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  bilans: {
    type: Number,
    default: 0
  },
  imageUrl: { type: String, default: '/images/avatar.png' },
  games: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Game'
    }
  ]
})

module.exports = mongoose.model('User', userSchema)
