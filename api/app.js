const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const operacijeRoutes = require("./routes/operacije");
const authRoutes = require("./routes/auth");

const app = express();
const MONGODB_URL = "mongodb+srv://korisnik:1234asdf@proba-wu86q.gcp.mongodb.net/igraDB?retryWrites=true&w=majority";

app.use(bodyParser.json());

app.use("/images", express.static(path.join(__dirname, "images")));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS, GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization", "id");
  next();
});

app.use("/operacije", operacijeRoutes);
app.use("/auth", authRoutes);

app.use((error, req, res, next) => {
  console.log(error);
  const status = error.statusCode;
  const message = error.message;
  const data = error.data;
  res.status(status).json({ message: message, data: data });
});

mongoose
  .connect(MONGODB_URL, {
    useNewUrlParser: true
  })
  .then(result => {
    
  })
  .catch(err => console.log(err));

// export the server middleware
module.exports = {
  path: '/api',
  handler: app
}