const express = require("express");
const { body } = require("express-validator/check");
const multer = require("multer");

const isAuth = require("../middleware/is-auth");

const storage = multer.diskStorage({
  destination: function(req, res, cb) {
    cb(null, __dirname.replace("routes", "") + "images/");
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});
const operacijeController = require("../controllers/operacije");

const router = express.Router();


router.post("/bilans", operacijeController.bilans);


module.exports = router;
