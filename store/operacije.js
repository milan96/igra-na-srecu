import axios from 'axios'

export default {
  state() {
    return {
    }
  },
  getters: {},
  mutations: {
   
  },
  actions: {
    promenaStanja(vuexContext, data) {
      axios
        .post(vuexContext.rootState.auth.baseUrl + '/operacije/bilans', data)
        .then(response => {
          vuexContext.dispatch(
            'auth/promenaStanja',
            response.data.user.bilans,
            { root: true }
          )
        })
        .catch(err => {
          console.log(err)
        })
    }
  }
}
