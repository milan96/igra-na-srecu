import Vue from "vue"
import VueObserveVisibility from 'vue-observe-visibility'
import Footer from '~/components/footer'


Vue.use(VueObserveVisibility);
Vue.config.productionTip = false;
Vue.component('main-footer', Footer);